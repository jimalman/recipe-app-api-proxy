#!/bin/sh

# this file populates the uwsgi_params from a docker image
set -e # if any lines fail, return a failure and print error to the screen

# take template file, substitute the environment variables and output the resulting file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;' # don't run nginx in the background.  enables logs to be printed to docker output




